
let websocket = null

const EstablecerTexto = data => {
  const mensaje = `<div>${data}</div>`
  chat.insertAdjacentHTML('beforeend', mensaje)
}


const establecerMensaje= data=>{
    const msg=`<div><span>${data.nombre}</span>:<span>${data.mensaje}</span></div>`
    chat.insertAdjacentHTML('beforeend', msg)

}

btnConectar.addEventListener('click', e => {
  //ws: si es http - wss: si es https
  //registrar los eventos
  websocket = new WebSocket('ws://echo.websocket.org')
  websocket.onopen = () => EstablecerTexto('Conectado')
  websocket.onclose = () => EstablecerTexto('Desconectado')
  websocket.onerror = e => EstablecerTexto(e)
  websocket.onmessage = e => {
    const mensajeEnviado=JSON.parse(e.data)
    establecerMensaje(mensajeEnviado)
  }
})

btnDesconectar.addEventListener('click', e => {
  websocket.close()

})

btnEnviar.addEventListener('click', e => {
  const mensajePaEnviar={
      nombre:txtNombre.value,
      mensaje:txtMensaje.value
  }
  websocket.send(JSON.stringify(mensajePaEnviar))//enviar el objeto
})

